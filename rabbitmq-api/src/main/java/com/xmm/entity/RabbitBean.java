package com.xmm.entity;

import lombok.Data;

/**
 * @author xmm
 * @since 2020/2/24
 */
@Data
public class RabbitBean {
    Long id;
    String name;
}
