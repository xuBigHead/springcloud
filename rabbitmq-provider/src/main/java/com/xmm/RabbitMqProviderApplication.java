package com.xmm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author xmm
 */

@SpringBootApplication
public class RabbitMqProviderApplication {

    public static void main(String[] args) {
        SpringApplication.run(RabbitMqProviderApplication.class, args);
    }

}
