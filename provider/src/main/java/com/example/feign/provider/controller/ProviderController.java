package com.example.feign.provider.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author xmm
 */
@RestController
public class ProviderController {

    @GetMapping("provider")
    public void provider(){
        System.out.println("this is provider");
    }

    @GetMapping("returnString")
    public String returnString(){
        return "this is provider";
    }

}
