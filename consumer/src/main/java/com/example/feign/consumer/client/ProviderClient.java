package com.example.feign.consumer.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author xmm
 */
@Component
@FeignClient("feign-provider")
public interface ProviderClient {
    /**
     * 测试provider服务的无返回值方法
     */
    @GetMapping("/provider")
    void provider();

    /**
     * 测试provider服务的有返回值方法
     * @return string字符串
     */
    @GetMapping("/returnString")
    String returnString();
}
