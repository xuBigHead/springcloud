package com.example.feign.consumer.controller;

import com.example.feign.consumer.client.ProviderClient;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author xmm
 */
@RestController
@AllArgsConstructor
public class ConsumerController {

    ProviderClient providerClient;

    @GetMapping("/consumer")
    public void consumer(){
        System.out.println("this is consumer");
    }

    @GetMapping("/returnString")
    public String returnString(){
        return "this is consumer";
    }

    @GetMapping("/test")
    public void test(){
        providerClient.provider();
        String returnString = providerClient.returnString();
        System.err.println(returnString);
    }

}

